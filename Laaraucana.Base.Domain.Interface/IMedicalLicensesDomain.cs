﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laaraucana.Base.Domain.Entity;

namespace Laaraucana.Base.Domain.Interface
{
    public interface IMedicalLicensesDomain
    {
        Task<IEnumerable<MedicalLicense>> GetMedicalLicenseByAffiliateAsync(int affiliateId, int limit = 10, int offset = 0);
        Task<IEnumerable<MedicalLicense>> GetMedicalLicensesByAffiliatesAsync(IEnumerable<int> affiliateRuts);
    }
}
