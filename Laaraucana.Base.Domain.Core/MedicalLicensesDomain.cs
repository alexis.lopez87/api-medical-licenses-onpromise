﻿using System;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Domain.Interface;
using Laaraucana.Base.Infraestructura.Interface;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Laaraucana.Base.Domain.Core
{
    public class MedicalLicensesDomain: IMedicalLicensesDomain
    {
        private readonly IMedicalLicensesRepository _medicalLicensesRepository;

        public MedicalLicensesDomain(IMedicalLicensesRepository medicalLicensesRepository)
        {
            _medicalLicensesRepository = medicalLicensesRepository;

        }

        public async Task<IEnumerable<MedicalLicense>> GetMedicalLicenseByAffiliateAsync(int affiliateId, int limit = 10, int offset = 0)
        {
            var affiliateRut = int.Parse(affiliateId.ToString().Substring(0, affiliateId.ToString().Length - 1));
            var affiliateDV = int.Parse(affiliateId.ToString().Substring(affiliateId.ToString().Length - 1, 1));

            return await _medicalLicensesRepository.GetMedicalLicenseByAffiliateAsync(affiliateRut, affiliateDV, limit, offset);
        }

        public async Task<IEnumerable<MedicalLicense>> GetMedicalLicensesByAffiliatesAsync(IEnumerable<int> affiliateRuts)
        {
            return await _medicalLicensesRepository.GetMedicalLicenseByAffiliatesAsync(affiliateRuts);
        }

    }
}