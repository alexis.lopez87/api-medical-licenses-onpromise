﻿using System;
using AutoMapper;
using Laaraucana.Base.Application.DTO;
using Laaraucana.Base.Application.Interface;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Domain.Interface;
using Laaraucana.Base.Transversal.Common;
using System.Threading.Tasks;
using System.Collections.Generic;
namespace Laaraucana.Base.Application.Main
{
    public class MedicalLicensesApplication : IMedicalLicensesApplication
    {
        private readonly IMedicalLicensesDomain _medicalLicensesDomain;
        private readonly IMapper _mapper;
        private readonly IAppLogger<MedicalLicensesApplication> _logger;

        public MedicalLicensesApplication(IMapper mapper, IMedicalLicensesDomain medicalLicensesDomain, IAppLogger<MedicalLicensesApplication> logger)
        {
            _mapper = mapper;
            _medicalLicensesDomain = medicalLicensesDomain;
            _logger = logger;
        }


        public async Task<MedicalLicenseByAffiliateResponse> GetMedicalLicenseByAffiliateAsync(int affiliateId, int limit = 10, int offset = 0)
        {
            var response = new MedicalLicenseByAffiliateResponse();

            try
            {
                var medicalLicenses = await _medicalLicensesDomain.GetMedicalLicenseByAffiliateAsync(affiliateId, limit, offset);
                var medicalLicensesDataResponse = _mapper.Map<IEnumerable<MedicalLicensesDataResponse>>(medicalLicenses);

                response.data = medicalLicensesDataResponse;
                response.total = medicalLicenses.Count() == 0 ? 0 : medicalLicenses.First().total;
                response.limit = limit;
                response.offset = offset;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
            return response;
        }
        public async Task<IEnumerable<MedicalLicensesDataResponse>> GetMedicalLicensesByAffiliatesAsync(IEnumerable<int> affiliateRuts)
        {
            var response = new MedicalLicenseByAffiliateResponse();

            try
            {
                var medicalLicenses = await _medicalLicensesDomain.GetMedicalLicensesByAffiliatesAsync(affiliateRuts);
                var medicalLicensesDataResponse = _mapper.Map<IEnumerable<MedicalLicensesDataResponse>>(medicalLicenses);

                return medicalLicensesDataResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }
    }
}