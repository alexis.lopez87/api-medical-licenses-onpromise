﻿using System;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Infraestructura.Interface;
using Laaraucana.Base.Transversal.Common;
using Dapper;
using System.Data;
using System.Threading.Tasks;
using static Laaraucana.Base.Transversal.Common.Enums;

namespace Laaraucana.Base.Infraestructura.Repository
{
    public class MedicalLicensesRepository: IMedicalLicensesRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public MedicalLicensesRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        private class MedicalLicenseTemp
        {
            public int STARTDATE { get; set; }
            public int ENDDATE { get; set; }
            public string MEDICALLICENSENUMBER { get; set; }
            public string status { get; set; }
            public int TOTAL { get; set; }
            public int AFIRUT { get; set; }
            public string AFIRUTDV { get; set; }
        }

        public async Task<IEnumerable<MedicalLicense>> GetMedicalLicenseByAffiliateAsync(int affiliateRut, int affiliateDV, int limit = 10, int offset = 0)
        {
            using(var connection = _connectionFactory.GetConnection)
            {
                var sql = @"SELECT 
                                   LIC.LICDESFEC AS STARTDATE, 
	                               LIC.LICHASFEC AS ENDDATE,
                                   (SELECT DISTINCT(LM11.NUMIMPRE)
									  FROM LIEXP.ILFE002R LM11
									  WHERE LM11.AFIRUT = LIC.AFIRUT  
										AND LM11.AFIRUTDV = LIC.AFIRUTDV 
										AND LM11.NUMIMPRELA = LIC.LICIMPNUM
									AND EXISTS (
										SELECT 1
										 FROM LIEXP.ILFE002R LM12
										 WHERE LM12.AFIRUT = LIC.AFIRUT  
										   AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										   AND LM12.NUMIMPRELA = LIC.LICIMPNUM
									)
				
									UNION ALL
				
									SELECT DISTINCT(LM21.NUMIMPRE)
									  FROM LIEXP.ILF1010 LM21
									  WHERE LM21.AFIRUT = LIC.AFIRUT  
										AND LM21.LICIMPNUM = LIC.LICIMPNUM
									AND NOT EXISTS (
										SELECT 1
										  FROM LIEXP.ILFE002R LM12
										 WHERE LM12.AFIRUT = LIC.AFIRUT  
										   AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										   AND LM12.NUMIMPRELA = LIC.LICIMPNUM
									)
				
									UNION ALL
				
									SELECT DISTINCT(LM31.NUMIMPRE)
									  FROM LIEXP.ILF8600 LM31
									 WHERE LM31.AFIRUT = LIC.AFIRUT  
									   AND LM31.AFIRUTDV = LIC.AFIRUTDV 
									   AND LM31.LICIMPNUM = LIC.LICIMPNUM
									AND NOT EXISTS (
										SELECT 1
										 FROM LIEXP.ILFE002R LM12
										WHERE LM12.AFIRUT = LIC.AFIRUT  
										  AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										  AND LM12.NUMIMPRELA = LIC.LICIMPNUM
			        
										UNION 
			        
										SELECT 1
										 FROM LIEXP.ILF1010 LM13
										WHERE LM13.AFIRUT = LIC.AFIRUT  
										  AND LM13.LICIMPNUM = LIC.LICIMPNUM
									)
		    
									UNION ALL
			  
									SELECT DISTINCT(LM41.NUMIMPRE)
									  FROM LIEXP.ILF8610 LM41
									 WHERE LM41.AFIRUT = LIC.AFIRUT  
									   AND LM41.AFIRUTDV = LIC.AFIRUTDV 
									   AND LM41.LICIMPNUM = LIC.LICIMPNUM
										AND NOT EXISTS (
										SELECT 1
										 FROM LIEXP.ILFE002R LM12
										WHERE LM12.AFIRUT = LIC.AFIRUT  
										  AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										  AND LM12.NUMIMPRELA = LIC.LICIMPNUM
			        
										UNION 
			        
										SELECT 1
										 FROM LIEXP.ILF1010 LM13
										WHERE LM13.AFIRUT = LIC.AFIRUT  
										  AND LM13.LICIMPNUM = LIC.LICIMPNUM
			          
										UNION
			       
										SELECT 1
										 FROM LIEXP.ILF8600 LM14
										WHERE LM14.AFIRUT = LIC.AFIRUT  
										  AND LM14.AFIRUTDV = LIC.AFIRUTDV 
										  AND LM14.LICIMPNUM = LIC.LICIMPNUM
									)
		                            ) MEDICALLICENSENUMBER,
                                    (
	   	                            SELECT COUNT(AFIRUT)
	   	                              FROM LIEXP.ILF1000 
		                               WHERE LICEST = 2 
                                       AND LIC.LICOBS1 = 91
		                               AND AFIRUT = LIC.AFIRUT  
                                       AND AFIRUTDV = LIC.AFIRUTDV
	                               ) TOTAL
	                          FROM LIEXP.ILF1000 AS LIC
	                         WHERE LIC.LICEST = 2 
                               AND LIC.LICOBS1 = 91
	                           AND LIC.AFIRUT = ?
                               AND LIC.AFIRUTDV = ?
                               AND LIC.LICDESFEC >= TO_CHAR(CURRENT DATE - 5 YEARS, 'YYYYMMDD')
                             ORDER BY LIC.LICDESFEC DESC
                                   LIMIT ?
                                   OFFSET ?";

                var parameters = new { AffiliateRut = affiliateRut, AffiliateDV = affiliateDV, Limit = limit, Offset = offset };

                var results = await connection.QueryAsync<MedicalLicenseTemp>(sql, parameters);

                var medicalLicensesByAffiliate = new List<MedicalLicense>();

                if (results != null)
                {
                    foreach (var result in results)
                    {
                        var newMedicalLicense = new MedicalLicense()
                        {
                            medicalLicenseNumber = result.MEDICALLICENSENUMBER,
                            status = eStatusMedicalLicense.PendienteDeDocumentacion.GetDisplayName(),
                            startDate = new DateTime(int.Parse(result.STARTDATE.ToString().Substring(0, 4)), int.Parse(result.STARTDATE.ToString().Substring(4, 2)), int.Parse(result.STARTDATE.ToString().Substring(6, 2))),
                            endDate = new DateTime(int.Parse(result.ENDDATE.ToString().Substring(0, 4)), int.Parse(result.ENDDATE.ToString().Substring(4, 2)), int.Parse(result.ENDDATE.ToString().Substring(6, 2))),
                            total = result.TOTAL
                        };
                        medicalLicensesByAffiliate.Add(newMedicalLicense);
                    }
                }

                return medicalLicensesByAffiliate;
            }
        }

        public async Task<IEnumerable<MedicalLicense>> GetMedicalLicenseByAffiliatesAsync(IEnumerable<int> affiliateRuts)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sql = @"SELECT 
								   LIC.AFIRUT,
								   LIC.AFIRUTDV,
                                   (SELECT DISTINCT(LM11.NUMIMPRE)
									  FROM LIEXP.ILFE002R LM11
									  WHERE LM11.AFIRUT = LIC.AFIRUT  
										AND LM11.AFIRUTDV = LIC.AFIRUTDV 
										AND LM11.NUMIMPRELA = LIC.LICIMPNUM
									AND EXISTS (
										SELECT 1
										 FROM LIEXP.ILFE002R LM12
										 WHERE LM12.AFIRUT = LIC.AFIRUT  
										   AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										   AND LM12.NUMIMPRELA = LIC.LICIMPNUM
									)
				
									UNION ALL
				
									SELECT DISTINCT(LM21.NUMIMPRE)
									  FROM LIEXP.ILF1010 LM21
									  WHERE LM21.AFIRUT = LIC.AFIRUT  
										AND LM21.LICIMPNUM = LIC.LICIMPNUM
									AND NOT EXISTS (
										SELECT 1
										  FROM LIEXP.ILFE002R LM12
										 WHERE LM12.AFIRUT = LIC.AFIRUT  
										   AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										   AND LM12.NUMIMPRELA = LIC.LICIMPNUM
									)
				
									UNION ALL
				
									SELECT DISTINCT(LM31.NUMIMPRE)
									  FROM LIEXP.ILF8600 LM31
									 WHERE LM31.AFIRUT = LIC.AFIRUT  
									   AND LM31.AFIRUTDV = LIC.AFIRUTDV 
									   AND LM31.LICIMPNUM = LIC.LICIMPNUM
									AND NOT EXISTS (
										SELECT 1
										 FROM LIEXP.ILFE002R LM12
										WHERE LM12.AFIRUT = LIC.AFIRUT  
										  AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										  AND LM12.NUMIMPRELA = LIC.LICIMPNUM
			        
										UNION 
			        
										SELECT 1
										 FROM LIEXP.ILF1010 LM13
										WHERE LM13.AFIRUT = LIC.AFIRUT  
										  AND LM13.LICIMPNUM = LIC.LICIMPNUM
									)
		    
									UNION ALL
			  
									SELECT DISTINCT(LM41.NUMIMPRE)
									  FROM LIEXP.ILF8610 LM41
									 WHERE LM41.AFIRUT = LIC.AFIRUT  
									   AND LM41.AFIRUTDV = LIC.AFIRUTDV 
									   AND LM41.LICIMPNUM = LIC.LICIMPNUM
										AND NOT EXISTS (
										SELECT 1
										 FROM LIEXP.ILFE002R LM12
										WHERE LM12.AFIRUT = LIC.AFIRUT  
										  AND LM12.AFIRUTDV = LIC.AFIRUTDV 
										  AND LM12.NUMIMPRELA = LIC.LICIMPNUM
			        
										UNION 
			        
										SELECT 1
										 FROM LIEXP.ILF1010 LM13
										WHERE LM13.AFIRUT = LIC.AFIRUT  
										  AND LM13.LICIMPNUM = LIC.LICIMPNUM
			          
										UNION
			       
										SELECT 1
										 FROM LIEXP.ILF8600 LM14
										WHERE LM14.AFIRUT = LIC.AFIRUT  
										  AND LM14.AFIRUTDV = LIC.AFIRUTDV 
										  AND LM14.LICIMPNUM = LIC.LICIMPNUM
									)
		                            ) MEDICALLICENSENUMBER
	                          FROM LIEXP.ILF1000 AS LIC
	                         WHERE LIC.LICEST = 2 
                               AND LIC.LICOBS1 = 91
	                           AND LIC.AFIRUT IN ?
                               AND LIC.LICDESFEC >= TO_CHAR(CURRENT DATE - 5 YEARS, 'YYYYMMDD')";

                var parameters = new { AffiliateRuts = affiliateRuts };

                var results = await connection.QueryAsync<MedicalLicenseTemp>(sql, parameters);

                var medicalLicensesByAffiliate = new List<MedicalLicense>();

                if (results != null)
                {
                    foreach (var result in results)
                    {
                        var newMedicalLicense = new MedicalLicense()
                        {
                            medicalLicenseNumber = result.MEDICALLICENSENUMBER,
                            rut = result.AFIRUT,
							rutDV = result.AFIRUTDV
                        };
                        medicalLicensesByAffiliate.Add(newMedicalLicense);
                    }
                }

                return medicalLicensesByAffiliate;
            }
        }


    }
}