﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laaraucana.Base.Domain.Entity;
namespace Laaraucana.Base.Infraestructura.Interface
{
    public interface IMedicalLicensesRepository
    {
        #region Métodos Asincronos
        Task<IEnumerable<MedicalLicense>> GetMedicalLicenseByAffiliateAsync(int affiliateRut, int affiliateDV, int limit = 10, int offset = 0);
        Task<IEnumerable<MedicalLicense>> GetMedicalLicenseByAffiliatesAsync(IEnumerable<int> affiliateRuts);

        #endregion

    }
}
