﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;
using Laaraucana.Base.Application.DTO;
using Laaraucana.Base.Application.Interface;
using System.Security.Cryptography;

namespace Laaraucana.Base.Services.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalLicensesController : Controller
    {
        private readonly IMedicalLicensesApplication _medicalLicensesApplication;

        public MedicalLicensesController(IMedicalLicensesApplication medicalLicensesApplication)
        {
            _medicalLicensesApplication = medicalLicensesApplication;            
        }

        /// <summary>
        /// Obtiene el estado de la licencia medicas por afiliado.
        /// </summary>        
        /// <returns></returns>
        /// 
        [HttpGet("affiliate/{affiliateId}")]        
        public async Task<IActionResult> GetMedicalLicenseByAffiliateAsync(int affiliateId, [FromQuery] QueryParameterGet queryParameter)
        {
            var limit = queryParameter.limit;
            var offset = queryParameter.page - 1;
            var response = await _medicalLicensesApplication.GetMedicalLicenseByAffiliateAsync(affiliateId, limit, offset);

            return Ok(response);       
        }

        [HttpGet("affiliates")]
        public async Task<IActionResult> GetMedicalLicenseByAffiliateAsync([FromQuery] ListAffiliates queryParameter)
        {
            var response = await _medicalLicensesApplication.GetMedicalLicensesByAffiliatesAsync(queryParameter.rut);

            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetMenssageAsync()
        {
            return Ok("Primera prueba");
        }

    }
}
