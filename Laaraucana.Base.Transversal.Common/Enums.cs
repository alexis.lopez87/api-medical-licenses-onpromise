﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laaraucana.Base.Transversal.Common
{
    public class Enums
    {
        public enum eStatusMedicalLicense
        {
            [Description("Recepcionada")]
            Recepcionada,
            [Description("Devuelta al empleador")]
            DevueltaAlEmpleador,
            [Description("No tramitada por empleador")]
            NoTramitadaPorEmpleador,
            [Description("Anulada")]
            Anulada,
            [Description("En proceso de pronunciamiento COMPIN")]
            EnProcesoDePronunciamientoCOMPIN,
            [Description("Autorizada por COMPIN")]
            AutorizadaPorCOMPIN,
            [Description("Rechazada por COMPIN")]
            RechazadaPorCOMPIN,
            [Description("Pendiente de documentación COMPIN")]
            PendienteDeDocumentacionCOMPIN,
            [Description("En análisis de proceso de pago")]
            EnAnalisisDeProcesoDePago,
            [Description("Pendiente de documentación")]
            PendienteDeDocumentacion,
            [Description("Autorizada para pago")]
            AutorizadaParaPago,
            [Description("Autorizada a pago empresa")]
            AutorizadaaPagoEmpresa,
            [Description("Sin derecho a subsidio por LM menor o igual a 3 días")]
            SinDerechoSubsidioLMMenorOIgual3Dias,
            [Description("Sin derecho a subsidio")]
            SinDerechoASubsidio,
            [Description("Devuelta a COMPIN / Devuelta a otra Caja")]
            DevueltaCOMPINOOtraCaja,
            [Description("Prescrita")]
            Prescrita,
            [Description("Pagada")]
            Pagada,
            [Description("Transferencia rechazada")]
            TransferenciaRechazada,
            [Description("Licencia médica pendiente de cobro")]
            LicenciaMedicaPendienteDeCobro
        }
    }
}
