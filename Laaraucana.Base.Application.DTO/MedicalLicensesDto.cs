﻿namespace Laaraucana.Base.Application.DTO
{
    public class MedicalLicensesDto
    {
        public string medicalLicenseNumber { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string status { get; set; }
        public int total { get; set; }
    }
    public class MedicalLicensesDataResponse
    {
        public string medicalLicenseNumber { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string status { get; set; }
        public int rut { get; set; }
        public string rutDV { get; set; }
    }
    public class MedicalLicenseByAffiliateResponse
    {
        public int total { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public  IEnumerable<MedicalLicensesDataResponse> data { get; set; }
    }
    
}