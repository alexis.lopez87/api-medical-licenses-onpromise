﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laaraucana.Base.Application.DTO
{
    public class QueryParameterGet
    {
        public int limit { get; set; }
        public int page { get; set; }
    }
}
