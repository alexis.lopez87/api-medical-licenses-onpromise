﻿using System;
using AutoMapper;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Application.DTO;
namespace Laaraucana.Base.Transversal.Mapper
{
    public class MappingsProfile: Profile
    {
        public MappingsProfile()
        {
            CreateMap<MedicalLicense, MedicalLicensesDto>().ReverseMap();
            CreateMap<MedicalLicense, MedicalLicensesDataResponse>().ReverseMap();
        }

    }
}