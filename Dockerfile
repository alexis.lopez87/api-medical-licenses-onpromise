FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /source

# copy csproj and restore as distinct layers
COPY *.sln .
COPY ./Laaraucana.Base.Application.DTO/*.csproj ./Laaraucana.Base.Application.DTO/
COPY ./Laaraucana.Base.Application.Interface/*.csproj ./Laaraucana.Base.Application.Interface/
COPY ./Laaraucana.Base.Application.Main/*.csproj ./Laaraucana.Base.Application.Main/
COPY ./Laaraucana.Base.Domain.Core/*.csproj ./Laaraucana.Base.Domain.Core/
COPY ./Laaraucana.Base.Domain.Entity/*.csproj ./Laaraucana.Base.Domain.Entity/
COPY ./Laaraucana.Base.Domain.Interface/*.csproj ./Laaraucana.Base.Domain.Interface/
COPY ./Laaraucana.Base.Infraestructura.Data/*.csproj ./Laaraucana.Base.Infraestructura.Data/
COPY ./Laaraucana.Base.Infraestructura.Interface/*.csproj ./Laaraucana.Base.Infraestructura.Interface/
COPY ./Laaraucana.Base.Infraestructura.Repository/*.csproj ./Laaraucana.Base.Infraestructura.Repository/
COPY ./Laaraucana.Base.Services.WebApi/*.csproj ./Laaraucana.Base.Services.WebApi/
COPY ./Laaraucana.Base.Transversal.Common/*.csproj ./Laaraucana.Base.Transversal.Common/
COPY ./Laaraucana.Base.Transversal.Logging/*.csproj ./Laaraucana.Base.Transversal.Logging/
COPY ./Laaraucana.Base.Transversal.Mapper/*.csproj ./Laaraucana.Base.Transversal.Mapper/
RUN pwd && ls -l
RUN dotnet restore Laaraucana.Base.WebApi.sln
COPY . ./
RUN dotnet publish -c release -o /app

# final stage/image
FROM mcr.microsoft.com/dotnet/nightly/aspnet:6.0
#RUN apt-get update && apt-get install -y --no-install-recommends libldap-2.4-2 && rm -rf /var/lib/apt/lists/*

# install odbc drivers
RUN apt-get update
RUN apt-get install -y odbcinst1debian2 libodbc1 odbcinst unixodbc && apt-get install -y libsasl2-modules-gssapi-mit
RUN apt-get install -y unzip
RUN apt-get install -y vim
RUN apt-get install -y wget 
RUN apt-get install -y gawk
RUN apt-get install -y curl
RUN apt-get install -y coreutils  

RUN curl https://public.dhe.ibm.com/software/ibmi/products/odbc/debs/dists/1.1.0/ibmi-acs-1.1.0.list | tee /etc/apt/sources.list.d/ibmi-acs-1.1.0.list
RUN apt-get update && apt-get install -y ibm-iaccess

RUN gawk -i inplace '{ print } ENDFILE { print "[ISeries Access]" }'  /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "Description=iSeries Access" }'  /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "Driver=IBM i Access ODBC Driver" }' /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "System=TESTDESA" }'  /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "UserID=USRSISTQA" }'  /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "Password=USRSISTQA" }'  /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "DefaultLibraries=MYLIB" }'  /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "Naming=0" }'  /etc/odbc.ini
RUN gawk -i inplace '{ print } ENDFILE { print "TrueAutoCommit=1" }'  /etc/odbc.ini

RUN cat /etc/odbc.ini

WORKDIR /app
COPY --from=build /app ./

EXPOSE 80
EXPOSE 443
ENTRYPOINT ["dotnet", "Laaraucana.Base.Services.WebApi.dll"]
