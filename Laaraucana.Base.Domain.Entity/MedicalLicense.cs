﻿namespace Laaraucana.Base.Domain.Entity
{
    public class MedicalLicense
    {
        //agregar las entidas con sus atributos

        public string medicalLicenseNumber { get; set; }
        public DateTime startDate { get; set; }  
        public DateTime endDate { get; set; }
        public string status { get; set; }
        public int total { get; set; }
        public int rut { get; set; }
        public string rutDV { get; set; }
    }
}