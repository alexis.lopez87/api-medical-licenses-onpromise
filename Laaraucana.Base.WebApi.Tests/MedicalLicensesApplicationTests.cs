using AutoMapper;
using FluentAssertions;
using Laaraucana.Base.Application.DTO;
using Laaraucana.Base.Application.Main;
using Laaraucana.Base.Domain.Entity;
using Laaraucana.Base.Domain.Interface;
using Laaraucana.Base.Transversal.Common;
using Moq;

namespace Laaraucana.Base.WebApi.Tests
{
    public class MedicalLicensesApplicationTests : IDisposable
    {
        Mock<IMedicalLicensesDomain> mockDomain;
        Mock<IMapper> mockMapper;
        Mock<IAppLogger<MedicalLicensesApplication>> mockLogger;

        int affiliateId = 0;
        int limit = 0;
        int offset = 0;

        public MedicalLicensesApplicationTests()
        {
            mockDomain = new Mock<IMedicalLicensesDomain>();
            mockMapper = new Mock<IMapper>();
            mockLogger = new Mock<IAppLogger<MedicalLicensesApplication>>();

            affiliateId = 1;
            limit = 10;
            offset = 0;
        }

        public void Dispose()
        {
            mockDomain.Reset();
            mockMapper.Reset();
            mockLogger.Reset();

            affiliateId = 0;
            limit = 0;
            offset = 0;
        }

        [Fact]
        public async Task GetMedicalLicenseByAffiliateAsync_GetANumberOfMedicalLicensesAsync()
        {
            IEnumerable<MedicalLicense> medicalLicenses = new List<MedicalLicense>
            {
                new MedicalLicense() { medicalLicenseNumber = "1", startDate = DateTime.Now, endDate = DateTime.Now.AddDays(1), status = "Pendiente", total = 1 }
            };
            IEnumerable<MedicalLicensesDataResponse> medicalLicensesDataResponse = new List<MedicalLicensesDataResponse>
            {
                new MedicalLicensesDataResponse() {

                    medicalLicenseNumber = medicalLicenses.First().medicalLicenseNumber,
                    startDate = medicalLicenses.First().startDate,
                    endDate = medicalLicenses.First().endDate,
                    status = medicalLicenses.First().status
                }
            };

            mockDomain.Setup(x => x.GetMedicalLicenseByAffiliateAsync(affiliateId, limit, offset)).ReturnsAsync(medicalLicenses);
            mockMapper.Setup(x => x.Map<IEnumerable<MedicalLicensesDataResponse>>(medicalLicenses)).Returns(medicalLicensesDataResponse);

            var medicalLicenseApp = new MedicalLicensesApplication(mockMapper.Object, mockDomain.Object, mockLogger.Object);
            var medicalLicensesByAffiliate = await medicalLicenseApp.GetMedicalLicenseByAffiliateAsync(affiliateId);

            
            var medicalLicensesResponse = new MedicalLicenseByAffiliateResponse() { limit = limit, offset = offset, total = medicalLicenses.First().total, data = medicalLicensesDataResponse };

            medicalLicensesByAffiliate.Should().BeOfType<MedicalLicenseByAffiliateResponse>();
            medicalLicensesByAffiliate.Should().BeEquivalentTo(medicalLicensesResponse);
            medicalLicensesByAffiliate.data.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetMedicalLicenseByAffiliateAsync_GetZeroLicensesAsync()
        {
            IEnumerable<MedicalLicense> medicalLicenses = new List<MedicalLicense>();
            mockDomain.Setup(x => x.GetMedicalLicenseByAffiliateAsync(affiliateId, limit, offset)).ReturnsAsync(medicalLicenses);
            IEnumerable<MedicalLicensesDataResponse> medicalLicensesDataResponse = new List<MedicalLicensesDataResponse>();
            mockMapper.Setup(x => x.Map<IEnumerable<MedicalLicensesDataResponse>>(medicalLicenses)).Returns(medicalLicensesDataResponse);

            var medicalLicenseApp = new MedicalLicensesApplication(mockMapper.Object, mockDomain.Object, mockLogger.Object);
            var medicalLicensesByAffiliate = await medicalLicenseApp.GetMedicalLicenseByAffiliateAsync(affiliateId);

            var medicalLicensesResponse = new MedicalLicenseByAffiliateResponse() { limit = limit, offset = offset, total = 0, data = medicalLicensesDataResponse };

            medicalLicensesByAffiliate.Should().BeOfType<MedicalLicenseByAffiliateResponse>();
            medicalLicensesByAffiliate.Should().BeEquivalentTo(medicalLicensesResponse);
            medicalLicensesByAffiliate.data.Should().HaveCount(0);
        }
    }
}