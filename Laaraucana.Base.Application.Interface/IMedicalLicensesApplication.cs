﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laaraucana.Base.Application.DTO;
using Laaraucana.Base.Transversal.Common;
namespace Laaraucana.Base.Application.Interface
{
    public interface IMedicalLicensesApplication
    {
        Task<MedicalLicenseByAffiliateResponse> GetMedicalLicenseByAffiliateAsync(int affiliateId, int limit = 10, int offset = 0);
        Task<IEnumerable<MedicalLicensesDataResponse>> GetMedicalLicensesByAffiliatesAsync(IEnumerable<int> affiliateRuts);

    }
}
